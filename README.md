# Lerna Tutorial

[![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lernajs.io/)

[`Lerna`](https://github.com/lerna/lerna) is a tool that optimizes the workflow around managing multi-package repositories with git and npm

## Prerequisites

First of all, we need to install [`Node`](https://nodejs.org/es/). To check if you have it already installed run the following command:
```
node -v
```

## Tutorial

For this tutorial we'll need to install Lerna first. The version we are going to use is `2.0.0-beta.20`
```
npm i lerna@2.0.0-beta.20 -g
```

Also we need to create a folder for our project. Let's name it `lerna-tutorial`: 
```
mkdir lerna-tutorial
```

Get into our new folder:
```
cd lerna-tutorial 
```

Initialize Lerna. This will create a `packages` folder, a `lerna.json` file and will update the `packages.json` file:
```
lerna init
```

We are going to create two packages. First, we're going to create `projectOne`. Follow these steps to do it:
```
cd packages
mkdir projectOne
cd projectOne
npm init -y
echo "module.exports = 'Project One'" > index.js
```

Follow these steps for `projectTwo`. It's the same as for `projectOne`:
```
cd ..
mkdir projectTwo
cd projectTwo
npm init -y
echo "module.exports = 'Project Two'" > index.js
```

Now we're going to create a `wrapper` package that will use both `projectOne` and `projectTwo`:
```
cd ..
mkdir wrapper
cd wrapper
npm init -y
touch index.js
```

Open up `/packages/wrapper/index.js` and set up the following lines:
```js
var projectOne = require('projectOne');
var projectTwo = require('projectTwo');
console.log("This project wraps up '" + projectOne + "' and '" + projectTwo + "'");
```

Now we need to set up our `dependencies` in the `package.json`, inside the `wrapper` folder:
```json
{
  ...
  "dependencies": {
    "projectOne": "1.0.0",
    "projectTwo": "1.0.0"  
  }
  ...
}
```

After these steps we need to run the following command:
```
lerna bootstrap
```

We should see something like this:
```
Lerna v2.0.0-beta.20
Linking all dependencies
Successfully bootstrapped 3 packages.
```

And with this we end this tutorial. If you want to test that everything is right, run `node ./packages/wrapper/index.js` you'll get this output:
```
This project wraps up 'Project One' and 'Project Two'
```